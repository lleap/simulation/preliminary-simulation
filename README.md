# Preliminary Exoskeleton Simulation

This repository is for the ROS-based exoskeleton simulation developed by LLEAP, a Cal Poly club project. The simulated Gazebo model of our biped, lower-limb exoskeleton is based on custom SolidWorks designs. The goal of the simulation is to provide a test bed for control code, and to root out design flaws before the expense of manufacturing. This is still a very preliminary simulation, although work is underway to flush out geometries and physical properties. See below for details on how to download, run, and control our simulation.

## Prerequisites
- ROS Noetic
- ros_control 
- Gazebo Sim
- A catkin workspace
- git
- A GitLab account connected to our group

## Project Structure
The project is organized into a few different ROS packages, namely the highest level directories you see listed above. Packages and their contents are split by their function for modularity and ease of simultaneous development. Each package name is prefixed with `lleap_exo_` followed by the appropriate, function-specific suffix as follows:
* `lleap_exo_description` describes the physical model of our robot. This package contains the URDF/xacro files, mesh files (from SolidWorks), and related physical property files.
* `lleap_exo_launch` contains the launch files and associated launch config files (e.g. Rviz layout, ros_contol descriptors).
* Other packages to come include `lleap_exo_src` which will contain all source code (i.e. ROS nodes), and possibly `lleap_exo_msgs` to contain our custom messages and be responsible for their generation.

## Downloading the project
1. Launch a terminal window and navigate to the `src` folder of your catkin workspace.

2. To download this project (essentially all of its packages), type the following into the command line: 
    ```
    git clone git@gitlab.com:lleap/simulation/preliminary-simulation.git
    ```
3. Back out of the `src` folder into the top level of your catkin workspace.
    ```
    cd ..
    ```
4. Build the package (i.e. make it runnable) by typing the following into terminal:
    ```
    catkin_make
    ```
    The `lleap_exo_XXX` packages should now be ready and runnable.  

    You can control the simulation through the terminal or with `rqt`.
  

## Controlling the simulation through the terminal

1. Launch a terminal window and navigate to your catkin workspace. Once there, type:
    ```
    source devel/setup.bash
    ```
2. In the same terminal window, type:
    ```
    roslaunch lleap_exo_launch gazebo.launch
    ```
    After a few seconds, the model should open in Gazebo.

3. Open a new terminal window and use `rostopic pub` to control each leg (joint group). For example, to control the left leg, type:
    ```
    rostopic pub -1 /lleap_exo/left_joint_group_position_controller/command std_msgs/Float64MultiArray "{data: [1.2, -0.5, 0.1]}"
    ```
    What is this doing?
    * The `-1` means only publish this message once. You could equivalently use `--once`.
    * It is publishing to the `/command` topic of the `/left_joint_group_position_controller` which is under the robot's namespace: `/lleap_exo`.
    * The message type is `std_msgs/Float64MultiArray`. You can find a description of this message type [here](http://docs.ros.org/en/noetic/api/std_msgs/html/msg/Float64MultiArray.html).
    * We only care about the `data` array which contains the actual commands. The syntax ("", :, {}, \[], commas) here are important. {} is because the message type does contain multiple things, but we are only choosing to fill part of it, thus we tell it this using notation as for a dictionary.
    * The target data itself is in \[] (meaning a list) because it is a `joint_GROUP_position_controller`. One target value for each joint in the group, *in order of appearance as in the config yaml file!* So: hip, knee, ankle.
    * Since the controller is a `joint_group_POSITION_controller`, the commands are joint 'positions', or angles in **radians**. They can be given as ints or floats. The values shown are just examples, but they can be any values within the limits of each joint which are given below in radians (units of control; rounded values since real values calculated directly from degrees) and degrees:
        * hip limits: \[-0.611, 2.199] in radians = \[-35, 126] in degrees
        * knee limits: \[-1.676, 0] in radians = \[-96, 0] in degrees
        * ankle limits: \[-0.785, 0.262] in radians = \[-45, 15] in degrees, although this joint is theoretically fixed


## Controlling the simulation with `rqt`

1. Open a terminal inside your catkin workspace and type:
    ```
    source devel/setup.bash
    ```
2. In the same terminal window, type:
    ```
    roslaunch lleap_exo_launch gazebo.launch
    ```
    After a few seconds, the model should open in Gazebo.
    
3. Open a new terminal window. Type
    ```
    rqt
    ```
    `rqt` should launch. 

4. Under the `Plugins` dropdown menu, click on `Topics > Message Publisher` if a `Message Publisher` window is not open by default (you can see the window name near the top left).

5. Select the `/lleap_exo/left_joint_group_position_controller/command` topic and click on the green plus button on the right. Repeat for the `/lleap_exo/right_joint_group_position_controller/command` topic.

6. Click on the dark arrow on the left of the newly added topics, and you will see a `data` row.

7. Control each leg by entering the desired joint positions (angle) in radians into the array \[] in the `expression` box. The command will be sent to the Gazebo simulation. For example, to make an exaggerated, sinusoidal walking gait:
    1. Pick one leg - left or right - to start with.
    2. Type or copy the following into the leg's corresponding `expression` box:
        ```
        [sin(i - 0.601) * 1.405 + 0.794, cos(i) * 0.838 - 0.838, sin(i + 2.618) * 0.524 - 0.262]
        ```
        * These equations move the hip, knee, and ankle joints (respectively) sinusoidally. The `i` is the time step variable in `rqt`, and the constants are chosen so that each joint oscillates from their max to min positions.
        * Amplitude scales the sinusoid to the correct range. It is equal to half of the full joint range.
        * Vertical shift is the offset to shift the sinusoid's min and max values (by shifting the center line) into the correct range so it doesn't simply oscillate between positive and negative amplitude.
        * Phase (or horizontal) shift places the resting position (angle = 0) at time = 0. These values were found by calculating the other two (amplitude and vertical shift), plotting the function (e.g., using Desmos), and then taking the x-coordinate of the nearest x-intercept (i.e., when the output angle, aka y-value = 0).
        * For the knee, cosine is used instead of sine since the knee's (current) maximum is already angle = 0. It could be replaced by sine with a phase shift of pi/2.
        * For the ankle, the phase shift shifts to the 2nd x-intercept to start the foot extending rather than flexing.
    3. Hit "Enter" or simply click out of the text box.
    4. You can change the value in the `rate` (in Hz) box to make the simulation update faster (higher number) or slower (lower number). Hit "Enter" or simply click out of the text box.
        * You can change this rate later to see for yourself.
    5. Select the empty checkbox to the left of the leg's topic name. You should see your chosen leg start moving in Gazebo!
    6. Repeat steps 2-4 for the opposite leg, but then wait to click the checkbox so you don't set it in motion until the already moving leg is at its opposite peak. This will garner the effect of a normal bipedal gait! You can uncheck one or both topics at any time to stop publishing and see that joint freeze. Try it out!

