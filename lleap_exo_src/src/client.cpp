#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <my_robot/RobotAction.h>

using namespace my_robot;   // so don't have to always include my_robot::RobotXXX
typedef actionlib::SimpleActionClient<RobotAction> Client;  // convenience

class MyNode
{
private:
    Client ac;  // action client instance
    // TODO: pub/sub instance; whatever needs to be done to react to service call that says when new param server goal is there
    //^ instead, look at param caching (http://wiki.ros.org/roscpp/Overview/Parameter%20Server), i.e. can be notified of when param changes
    /**
    Otherwise, general arch is this:
    - some params w/ goal (single pose)
    - "service call"(?), i.e. user manually sends "true" msg or something along a spc (created for this purpose) topic
    - this node subscribes to (and polls) that topic, waiting for the "true" msg to come through so it knows when to look at param server
        - OR check out the caching thing mentioned above
    - get goal from PS
    - send goal to action server
    - server accepts goal
    - server (in goalCB still) pubs that goal to ros_control /command topic
        - OR the client does this before (or after?) sending goal to server

    OR completely different plan:
    - go straight to figuring out traj controller which uses actionlib inherently
    - just pub to the /command topic, listen to the /state topic (this is all a built-in action server!)
    */
public:
    // constructor
    MyNode() : ac("my_robot", true)
    {
        // TODO: wait for service call to get goal from param server

        ROS_INFO("Waiting for action server to start.");
        ac.waitForServer();
        ROS_INFO("Action server started, sending goal.");
    }

    // what to call in main()
    void doStuff(int order)
    {
        // TODO: handle goal (gotten from param server) correctly
        RobotGoal goal;
        goal.order = order;

        // Need boost::bind to pass in the 'this' pointer
        ac.sendGoal(goal,
                    boost::bind(&MyNode::doneCb, this, _1, _2),
                    Client::SimpleActiveCallback(),             // TODO: figure out how to properly boost::bind Cb's below
                    Client::SimpleFeedbackCallback());

    }
    
    // Called once when the goal completes
    void doneCb(const actionlib::SimpleClientGoalState& state,
                const RobotResultConstPtr& result)
    {
        ROS_INFO("Finished in state [%s]", state.toString().c_str());
        ROS_INFO("Answer: %i", result->sequence.back());
        ros::shutdown();
    }

    // Called once when the goal becomes active
    void activeCb()
    {
        ROS_INFO("Goal just went active");
    }

    // Called every time feedback is received for the goal
    void feedbackCb(const RobotFeedbackConstPtr& feedback)
    {
        // TODO: print out fields of the FB msg
        ROS_INFO("Got Feedback of length %lu", feedback->sequence.size());
    }
};

int main (int argc, char **argv)
{
    ros::init(argc, argv, "my_robot_client");
    MyNode my_node;     // waits for goal, then waits for server
    my_node.doStuff(10);
    ros::spin();
    return 0;
}