#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <actionlib/server/simple_action_server.h>
#include <my_robot/RobotAction.h>

#include <assert.h>

// use namespace so don't have to always include my_robot::RobotXXX; can just do RobotXXX
using namespace my_robot;

class RobotServer
{
protected:
    ros::NodeHandle nh_;        // must declare nodehandle before server to avoid weird errors
    actionlib::SimpleActionServer<RobotAction> as_;     // the action server itself
    std::string action_name_;   // instance variable to hold action name (convenience)
    std::vector<double> goal_;  // instance variable to store received goal
    RobotFeedback feedback_;    // helper instance variable to update feedback given
    RobotResult result_;        // helper instance variable to hold the result before sending
    ros::Subscriber sub_;       // subscriber used to trigger updates
    double epsilon_;            // how close the robot must get to target to consider it "reached"
    double prevDist_;           // previous distance to target
    double hoverCount_;         // number of consecutive "hovering" iterations
    // TODO: indp service call handler to re-read these values from the param server
    double hoverDelta_;         // increment in distance that is consider hovering (slowed approach)
    int hoverLimit_;            // number of consecutive "hovering" iterations after which to stop trying

public:
    RobotServer(std::string name) : 
        as_(nh_, name, false),      // initialize action server (instance variable)
        action_name_(name)          // save the action name in the instance variable
    {
        // register the goal and feeback callbacks
        as_.registerGoalCallback(boost::bind(&RobotAction::goalCB, this));          // do when receives new goal
        as_.registerPreemptCallback(boost::bind(&RobotAction::preemptCB, this));    // do when goal overridden (e.g. by new goal)

        // update action feedback every time a new message is published to /joint_states
        sub_ = nh_.subscribe("/my_ur5/joint_states", 1, &RobotAction::analysisCB, this);

        as_.start();    // start the action server

        // TODO: get reachedTarget() epsilon and hovering() constants from param server
    }

    // private constructor? (TODO: lookup)
    ~RobotServer(void)
    {
    }

    // called once to accept a new goal
    void goalCB()
    {
        // reset helper variables
        prevDist_ = -1;
        hoverCount_ = 0;

        // accept the new goal
        goal_ = as_.acceptNewGoal()->target;
    }

    // respond quickly to a canceled action
    void preemptCB()
    {
        ROS_INFO("%s: Preempted", action_name_.c_str());
        // set the action state to preempted
        as_.setPreempted();
    }

    // called any time this node's subscriber receives a message
    void analysisCB(const std_msgs::Float32::ConstPtr& msg)
    {
        // make sure that the action hasn't been canceled
        if (!as_.isActive())
        {
            return;
        }
        
        // fill in and publish feedback (built-in "publish" channel)
        feedback_.pose = msg->position;     // grab the vector of positions (float64[6]) from the joint_states topic
        feedback_.error = error(goal_, feedback_.pose);
        feedback_.distance = distance(goal_, feedback_.pose)
        as_.publishFeedback(feedback_);

        // end if reached target or have been hovering for too long
        if(reachedTarget() || hovering()) 
        {
            // fill in result
            result_.pose = feedback_.pose;
            result_.error = feedback_.error;
            result_.distance = feedback_.distance;

            // based on the result, determine if successful or not
            if(reachedTarget())
            {
                ROS_INFO("%s: Aborted", action_name_.c_str());
                // set the action state to aborted
                as_.setAborted(result_);
            }
            else 
            {
                ROS_INFO("%s: Succeeded", action_name_.c_str());
                // set the action state to succeeded
                as_.setSucceeded(result_);
            }
        } 
    }

    // return if the target has been reached (i.e. current pose is within epsilon)
    bool reachedTarget()
    {
        return feedback_.distance <= epsilon_;
    }

    // checks to see if this iteration was considered hovering and updates instance variables accordingly
    // returns true if has been outside the epsilon of the target for too long
    bool hovering()
    {
        // if first iteration, skip hover check
        if (prevDist < 0)   // distance cannot be negative unless set as such on reset
        {
            return false;
        }

        double delta = prevDist_ - feedback_.distance;  // if getting closer, this should be positive

        // check if this is a "hovering" iteration
        if (delta <= hoverDelta_)
        {
            hoverCount_++;      // this iteration is hovering, so incr count
        }
        else
        {
            hoverCount_ = 0;    // no longer consecutive hovering
        }

        return hoverCount_ >= hoverLimit_;  // return if have been hovering for too long
    }

    // calculate the Euclidean distance between two n-D points; they must be of the same size
    static double distance(std::vector<double> a, std::vector<double> b)
    {
        assert(a.size() == b.size());

        double distSquared = 0;

        // calculate the sum of squares of all the differences
        for (int i = 0; i < a.size(); i++)
        {
            double targCoord = a[i];
            double currCoord = b[i];
            double difference = targCoord - currCoord;
            distSquared += std::pow(difference, 2);
        }

        return std::sqrt(distSquared);
    }

    // calculate the coordinate-by-coordinate error between two n-D points (a - b)
    // the two points must be the same size
    static std::vector<double> error(std::vector<double> a, std::vector<double> b)
    {
        assert(a.size() == b.size());

        std::vector<double> err;

        // calculate the error between each coordinate pair
        for (int i = 0; i < a.size(); i++)
        {
            err.push_back(a[i] - b[i]);
        }

        return err;
    }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "my_robot_server");

  RobotServer server(ros::this_node::getName());
  ros::spin();

  return 0;
}